# automapper-bundle

Integrate my [Automapper](https://gitlab.com/Etshy/automapper) library as a Bundle.

## Installation

`composer require etshy/automapper-bundle`

## Usage

Implements the `Etshy\AutoMapperBundle\EtshyAutoMapperConfiguratorInterface` to expose the `Etshy\AutoMapper\Configuration\AutoMapperConfiguration`.

Then you can register your mapping as state in the [base library](https://gitlab.com/Etshy/automapper)

To map objects, you have to inject `Etshy\AutoMapper\AutoMapperInterface` in your class.

This bundle is really early access !