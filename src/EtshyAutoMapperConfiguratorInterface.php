<?php

declare(strict_types=1);

namespace Etshy\AutoMapperBundle;

use Etshy\AutoMapper\Configuration\AutoMapperConfigurationInterface;

interface EtshyAutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigurationInterface $config): void;
}
