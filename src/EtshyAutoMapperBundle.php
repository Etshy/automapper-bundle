<?php

declare(strict_types=1);

namespace Etshy\AutoMapperBundle;

use Etshy\AutoMapperBundle\DependencyInjection\EtshyAutoMapperExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class EtshyAutoMapperBundle extends AbstractBundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new EtshyAutoMapperExtension();
    }
}
